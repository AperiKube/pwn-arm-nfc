#!/usr/bin/env python3
# -*- coding:Utf-8 -*-

#==========================================================#
# [+] Title:   NFC Listener                                #
# [+] Author:  Baptiste M. (Creased)                       #
# [+] Website: bmoine.fr                                   #
# [+] Email:   contact@bmoine.fr                           #
# [+] Twitter: @Creased_                                   #
#==========================================================#

# Generate documentation: epydoc -v --html nfc_listener.py -o ./docs

import argparse
import binascii
import socket
import struct
import json
import time
# import nfc
import re
import os

from subprocess import Popen, PIPE
from packages import log

__author__ = 'Baptiste MOINE <contact@bmoine.fr>'
__version__ = '0.1-dev'
__date__ = '27 January 2018'

DEFAULT_SOCKET_PATH = 'buffer.sock'
DEFAULT_SEND_DATA_INTERVAL = 5000
DEFAULT_WAIT_PULL_TAG = True

class Client(object):
    """This class allows to create a client."""
    def __init__(self, socket_path=DEFAULT_SOCKET_PATH):
        """Constructor of I{Client}.

        @param self: Current instance of I{Client}.
        @param socket_path: UNIX Socket path.

        @type self: C{Client}
        @type socket_path: C{str}

        @raise socket_path: TypeError, UNIX Socket path must be a valid string.
        @raise socket_path: OSError, UNIX Socket file not found.
        @raise socket_path: OSError, an error occured while trying to communicate with socket.
        """
        if not isinstance(socket_path, str) and not isinstance(socket_path, unicode):
            raise TypeError('UNIX Socket path must be a valid string!')
        elif not os.path.exists(socket_path):
            raise OSError('UNIX Socket file not found!')
        else:
            try:
                self.socket = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
                self.socket_path = socket_path
            except (OSError, AttributeError):
                raise OSError('An error occured while trying to communicate with socket!')

    def send(self, data):
        """Send data to the server.

        @param self: Current instance of I{Client}.
        @param data: Data to send through UNIX Socket.

        @type self: C{Client}
        @type data: C{str}

        @raise data: TypeError, Data must be a valid byte-string.
        @raise data: OSError, An error occured while trying to send data
        """
        if not isinstance(data, bytes):
            raise TypeError('Data must be a valid byte-string!')
        else:
            try:
                self.socket.connect(self.socket_path)
                self.socket.send(data)
                self.close()
            except OSError:
                raise OSError('An error occured while trying to send data.')
            else:
                log.success('Data has ben sent.')
                pass

    def restart(self):
        """Close socket communication.

        @raise socket: OSError, an error occured while trying to close socket communication.
        """
        try:
            self.close()
            self.start()
        except OSError:
            raise OSError('An error occured while trying to close socket communication.')
        else:
            return

    def start(self):
        """Start socket communication.

        @raise socket: OSError, an error occured while trying to close socket communication.
        """
        try:
            self.socket = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        except OSError:
            raise OSError('An error occured while trying to close socket communication.')
        else:
            return

    def close(self):
        """Close socket communication.

        @raise socket: OSError, an error occured while trying to close socket communication.
        """
        try:
            self.socket.close()
        except OSError:
            raise OSError('An error occured while trying to close socket communication.')
        else:
            return

def load_config(conf):
    """Load configuration from file.

    @param socket_path: UNIX Socket path.
    @type socket_path: C{Client}
    @raise socket_path: OSError, Specified configuration file not found.
    @raise socket_path: ValueError, An error occured while parsing configuration file.
    """
    config = dict()

    if conf:
        try:
            with open(conf, 'rt') as conf_fd:
                config = json.load(conf_fd)
                conf_fd.close()
        except OSError:
            log.error('Specified configuration file not found!')
        except ValueError:
            log.error('An error occured while parsing configuration file.')
        else:
            log.success('Loading configurations from {file}.'.format(file=conf))

    return config

# def poll(clf):
#     """Poll Contactless Frontend."""
#     try:
#         while True:
#             tag = clf.poll()
#             if tag:
#                 return tag
#             else:
#                 time.sleep(0.5)
#     except KeyboardInterrupt:
#         return None

def odd(str_):
    """Return if string length is odd."""
    return len(str_) % 2 == 1

def dump_tag():
    """Dump NFC tag."""
    cmd = ['/bin/bash', 'ressources/dump.sh']
    proc = Popen(cmd, stdout=PIPE)
    data = proc.communicate()[0]

    if data:
        data = data.split('54')[1][6:278]
        data = data[0:74] + data[106:202] + data[234:]
        if odd(data):
            data = '0' + data
        # log.debug(data)
        # data = ''.join([struct.pack('!H', int(''.join(c), 16)) for c in zip(data[0::2], data[1::2])])
        # data = data.decode('hex')
        data = binascii.unhexlify(data)
        # log.debug(data)
    return data

# def dump_tag(nfc_):
#     """Dump NFC tag."""
#     tag = poll(nfc_['clf'])
#     data = ''

#     if tag is not None and tag.ndef:
#         data = tag.ndef.message

#     return str(data)

def parse_args():
    """Arguments parsing."""
    parser = argparse.ArgumentParser(
        description='Alphanumeric blob to pixel v{version}'.format(
            version=__version__
        )
    )

    parser.add_argument('-c', '--config',
                        type=str,
                        default='ressources/config.json',
                        help='configuration file path')

    args = parser.parse_args()

    return args

def main():
    """Main process."""
    try:
        # Arguments parsing.
        args = parse_args()

        # Load config from args.config file.
        config = load_config(args.config)

        if 'send_data_inteval' not in config or not isinstance(config['send_data_inteval'], int):
            config['send_data_inteval'] = DEFAULT_SEND_DATA_INTERVAL

        # Instantiate a UNIX socket client.
        client = Client(config['socket_path'])

        # Start client process
        log.success('Client started.')
        log.info('Press CTRL+PAUSE to close the client.')

        # NFC stuff.
        # nfc_ = dict()

        if 'wait_pull_tag' not in config or not isinstance(config['wait_pull_tag'], bool):
            config['wait_pull_tag'] = DEFAULT_WAIT_PULL_TAG

        # if 'device' not in config or (not isinstance(config['device'], str) and not isinstance(config['device'], unicode)):
        #     raise ValueError('No contactless reader!')
        # else:
        #     for device in config['device']:
        #         try:
        #             nfc_['clf'] = nfc.ContactlessFrontend(str(config['device']))
        #             break
        #         except LookupError:
        #             pass

        log.info("Push tag.")

        while True:
            # log.info("Push tag.")

            # Retrieve data from NFC tag
            # tag_data = dump_tag(nfc_)
            tag_data = dump_tag()

            # if config['wait_pull_tag']:
            #     log.info("Pull tag.")
            #     while tag.is_present:
            #         time.sleep(1)

            if tag_data:
                try:
                    # Send data to UNIX socket
                    # client.send(tag_data.encode())
                    client.send(tag_data)
                except IOError:  # Server crashed, let's retry in few seconds
                    log.debug('Server seems to be crashed! Retrying...')
                    client.restart()

            time.sleep(config['send_data_inteval'] / 1000)

    except (ValueError, TypeError, OSError) as exception_:
        log.error(exception_)
    except KeyboardInterrupt:
        log.warn('Program has been stopped.')
        exit(1)

# Runtime processor.
if __name__ == '__main__':
    main()

