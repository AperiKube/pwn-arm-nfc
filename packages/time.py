"""Time beautifier package for Python.

This module helps to display human readable time units.

"""

__all__ = ['pretty_time']

__author__ = 'Baptiste MOINE <contact@bmoine.fr>'
__version__ = '0.1-dev'
__date__ = '05 September 2017'

######################################################################
## Main Functions
######################################################################

def pretty_time(seconds):
    """Print a human-readable time based on seconds.

    @param seconds: Number of seconds.
    @type seconds: C{int}
    """
    if not isinstance(seconds, int):
        raise TypeError('Seconds must be a valid integer!')
    else:
        time_arr = []

        units = [
            {
                'name': 'week',
                'seconds': 604800
            },
            {
                'name': 'day',
                'seconds': 86400
            },
            {
                'name': 'hour',
                'seconds': 3600
            },
            {
                'name': 'minute',
                'seconds': 60
            },
            {
                'name': 'second',
                'seconds': 1
            }
        ]

        for unit in units:
            count = int(seconds / unit['seconds'])

            if count > 0 or (unit['name'] == 'second' and len(time_arr) == 0):
                time_arr += ['{count} {unit}{s_unit}'.format(
                    count=count,
                    unit=unit['name'],
                    s_unit='' if count == 1 else 's'
                )]

            seconds = int(seconds % unit['seconds'])

        if len(time_arr) > 1:
            timestr = ' and '.join([', '.join(time_arr[:-1]), time_arr[-1]])
        else:
            timestr = time_arr[0]

        return timestr
