#!/bin/bash

# Will dump data in the NFC tag and stores them in "card.mfd"
# Parsing of hexdump results to fit with NFC parser in "nfc_listener.py"
mfoc -P 500 -O card.mfd &> /dev/null && hexdump -Cv card.mfd | cut -c9-59 | tr -d '\n '
